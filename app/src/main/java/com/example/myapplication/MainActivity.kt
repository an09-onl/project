package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.lang.Math.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        task3(4.0, 6.0)
        findZ(3.5, 3.5, 7.3)
        task4(3, 5, 6)
        task0(13, 5, 6)
        println(task3())



        var name= "Kate"

        print("""My name is
          $name
          My name contains
          ${name.length} letters""".trimMargin())
    }
}



fun findZ(a: Double, b: Double, c: Double) = ((a - 3) * b / 2) + c


//import kotlin.math.*
fun task3(x: Double = 1.0, y: Double = 4.0): Double {
    return (sin(x) + cos(y) / cos(x) - sin(y)) * tan(x * y)
}

fun task4(a: Int, b: Int, c: Int) {
    if ((a + b) <= c) {
        println("Does not exist")
    } else if ((a + c) <= b) {
        println("Does not exist")
    } else if ((b + c) <= a) {
        println("Does not exist")
    } else {
        println("exist")
    }
}

fun task0(a: Int, b: Int, c: Int) {
    when {
        c >= a + b || a >= c + b || b >= c + a -> {
            println("Does not exist")
        }
        else ->{
            println("exist")
        }
    }
}





